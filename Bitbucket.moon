--Bitbucket.moon
--depends: luasocket, luasec
socket  = require 'socket'
url     = require 'socket.url'
https   = require 'ssl.https'
json    = require 'dkjson.dkjson'
Printer = require 'Printer'
colors  = require 'ansicolors.ansicolors'

class Bitbucket
  new: (@config, @results = {}) =>
    --new Bitbucket
  login: () =>
    if not @config.project
      io.write "Project name:"
      @config.project = io.read!
    if not @config.login
      io.write "User name:"
      @config.login = io.read!
    -- surpress output
    os.execute("stty -echo") if os.getenv("SHELL")
    io.write "Password for #{@config.login}:"
    password = io.read!
    os.execute("stty echo") if os.getenv("SHELL")
    -- default request object
    @request =
      scheme: 'https',
      user: @config.login,
      password: password,
      host: "bitbucket.org",
      path: "/api/1.0/repositories/#{@config.login}/#{@config.project}/",
    if password == ""
      @request.password, @request.user = nil, nil
    @validateRepository!

  validateRepository: () =>
    command = url.build(@request)
    result, code = https.request(command)
    if code ~= 200
      print colors("%{red} [ x ] %{red}invalid or unauthorized repository")
    else
      print colors("%{green} [ o ] %{green}valid repository")

  postIssue: (issue, file) =>
    jsonIssue = {
      responsible: issue.assign
      priority: issue.priority
      status: issue.resolve
      content: "maitag: [#{issue.tag}] - #{file} | line #{issue.line}"
      kind: "task"
    }
    query = { "title=#{url.escape(issue.message .. ' [' .. file .. ']')}" }
    for k, v in pairs jsonIssue
      if v ~= ""
        query[#query + 1] = "&#{k}=#{url.escape(tostring(v))}"
    query = table.concat(query, "")
    r, c = https.request(@command, query)
    if c ~= 200
      print("Error:", r)

  post: () =>
    print "Submitting to https://bitbucket.org/#{@config.login}/#{@config.project}/issues"
    @request.path = "/api/1.0/repositories/#{@config.login}/#{@config.project}/issues/"
    @command = url.build(@request)
    -- post new issues, any issue without a resolve needs to be pushed?
    for file, tags in pairs @results
      for tag, issues in pairs tags
        if tag ~= "clean"
          for i, issue in pairs issues
            if not issue.resolve
              @postIssue(issue, file)

  getIssue: (issue) =>
    -- find tag and filepath in issue's contents
    _, _, tag, filepath, line = issue.content\find("^maitag:%s?%[(.-)%]%s?%-%s?(.-)%s|%sline%s(.-)$")
    if filepath
      assign = issue.responsible and issue.responsible.username
      _, _, title = issue.title\find("(.-)%s?%[")
      title = title or issue.title
      file = @results[filepath]
      if not file
        @results[filepath] = { clean: false }
        file = @results[filepath]
      file.clean = false
      if issue.priority == "" then issue.priorty = nil
      if not file[tag]
        file[tag] = { }
      file[tag][line..title] = {
        message: title
        priority: issue.priority
        resolve: issue.status
        assign: assign
        tag: tag
        line: tonumber(line)
      }

  get: () =>
    -- get issues
    @request.path = "/api/1.0/repositories/#{@config.login}/#{@config.project}/issues/"
    command = url.build(@request)

    res, code = https.request(command)
    res = json.decode(res)
    for i, issue in ipairs res.issues
      @getIssue(issue)

return Bitbucket
