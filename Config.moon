--Config.moon
require('pl.stringx').import!
file   = require 'pl.file'
pretty = require 'pl.pretty'

parseTags = (tags) ->
  if type(tags) == 'table'
    return tags
  else
    res = tags\split(';')
    res[1] = tags if #res == 0
    return res

defaultPath = '.maitag'
defaultConfig = [[{
  dirs = "",
  ignore = "",
  files = "",
  show = "all",
  sort = "a",
  tags = "todo;review;fix",
  priority = {
    critical = "red underline",
    major = "yellow",
    minor = "green dim"
  },
  remote = "Bitbucket",
  login = nil, -- username
  project = nil -- project name
}]]

class Config
  init: (arguments, defaults) =>
    defaults = defaults or {}
    --empty strings should be nil
    for k,v in pairs arguments
      arguments[k] = nil if arguments[k] == '' and defaults[k]
    @show = arguments.show or defaults.show or 'all'
    @show = 'all' if @show ~= 'all' and @show ~= 'dirty' and @show ~= 'clean'
    @sort     = arguments.sort or defaults.sort or 'a'
    @remote   = arguments.remote or defaults.remote
    @login    = arguments.login or defaults.login
    @project  = arguments.project or defaults.project
    @tags     = {k, true for k in *parseTags(arguments.tags or defaults.tags or "todo;review;fix")}
    @dirs     = parseTags(arguments.dirs or defaults.dirs or "")
    @ignore   = parseTags(arguments.ignore or defaults.ignore or "")
    @files    = parseTags(arguments.files or defaults.files or "")
    @priority = parseTags(arguments.priority or defaults.priority or {critical:'red', major: 'green', minor: 'blue'})
  new: (args) =>
    contents = file.read('.maitag')
    if contents
      @conf = assert(pretty.read(contents))
      @init(args, @conf)
    else
      @init(args)
      file.write('.maitag', defaultConfig)
  write: () =>
    if not @conf
      file.write(defaultPath, defaultConfig)
