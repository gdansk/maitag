local acolor = require('ansicolors.ansicolors')
local path = require('pl.path')
local config = nil
local files = nil
local orderedFiles = nil
local issuecount = nil
local modified = nil
local writer
do
  local _base_0 = io.stdout
  local _fn_0 = _base_0.write
  writer = function(...)
    return _fn_0(_base_0, ...)
  end
end
local removeKeys = nil
local alphabetic
alphabetic = function(a, b)
  return (a < b)
end
local countsort
countsort = function(a, b)
  return (issuecount[a] or 0) > (issuecount[b] or 0)
end
local modifiedsort
modifiedsort = function(a, b)
  return (modified[a] or 0) > (modified[b] or 0)
end
local colors
colors = function(s)
  return acolor(s, removeKeys)
end
local printf
printf = function(file, str)
  local out = files[file]
  out[#out + 1] = str
end
local format
format = function(character, color)
  return colors("%{" .. tostring(color) .. "}[ " .. tostring(character) .. " ]") .. colors(" %{" .. tostring(color) .. "}")
end
local printMessage
printMessage = function(file, priority, message, assign, resolve, line)
  local p = config.priority[priority] and colors("%{" .. tostring(config.priority[priority]) .. "}" .. tostring(priority) .. ": ") or ""
  local msg = "        " .. tostring(p) .. tostring(line) .. "\'" .. tostring(message) .. "\'" .. tostring(assign)
  msg = msg .. " " .. tostring(colors('%{green}' .. ((resolve and ' [' .. resolve .. ']') or '')))
  return printf(file, msg)
end
local printTag
printTag = function(file, tag)
  return printf(file, colors('    %{blue}' .. tag))
end
local printFile
printFile = function(file, clean, counts)
  files[file] = { }
  local count = counts[file]
  local postfix = count > 1 and "s" or ""
  local color = clean and format('o', 'green') or format('x', 'red')
  return printf(file, tostring(color) .. tostring(file) .. (count > 0 and "\t" .. tostring(format(count .. ' issue' .. postfix, 'cyan dim')) or ""))
end
local Printer
Printer = function(results, conf, counts, lastchanged, filewriter)
  removeKeys = filewriter
  filewriter = filewriter or writer
  files, orderedFiles = { }, { }
  local sort = alphabetic
  if conf.sort == 'c' then
    sort = countsort
  elseif conf.sort == 'm' then
    sort = modifiedsort
  end
  config = conf
  issuecount = counts
  modified = lastchanged or modified
  for file, tags in pairs(results) do
    if conf.show == "all" or (conf.show == "dirty" and not tags.clean) or (conf.show == "clean" and tags.clean) then
      orderedFiles[#orderedFiles + 1] = file
      printFile(file, tags.clean, counts)
      for tag, issues in pairs(tags) do
        if tag ~= 'clean' and conf.tags[tag] then
          printTag(file, "[ " .. tostring(tag) .. " ]: ")
          for k, issue in pairs(issues) do
            local assign = issue.assign and issue.assign ~= "" and colors("%{dim blue} " .. tostring(issue.assign))
            local line = issue.line and issue.line ~= "" and colors("%{dim}line " .. tostring(issue.line) .. " ")
            printMessage(file, issue.priority, issue.message, assign or "", issue.resolve, line or "")
          end
        end
      end
    end
  end
  table.sort(orderedFiles, sort)
  for i, filename in pairs(orderedFiles) do
    table.insert(files[filename], "")
    filewriter(table.concat(files[filename], '\n'))
  end
end
return Printer
