--Actions.moon
Printer   = require 'Printer'
Parse     = require 'Parser'
path      = require 'pl.path'
dir       = require 'pl.dir'
file      = require 'pl.file'
tablex    = require 'pl.tablex'

DEFAULT_SLEEP_TIME = 2 -- 2 seconds
SEPARATOR = package.config\sub(1,1)

getIssues = (iss) ->
  result = {}
  if not iss or iss.clean == nil return result
  for tag,v in pairs(iss)
    if tag ~= 'clean'
      for msg, m in pairs(v)
        result[#result+1] = m
  return result

compareIssue = (a, b) ->
  a.priority == b.priority and a.tag == b.tag and a.assign == b.assign and a.message == b.message

cleanIssue = (results, result, basename) ->
  -- if the file has no issues that are different, it's clean! Do not bother showing it again.
  if tablex.compare_no_order(getIssues(result), getIssues(results[basename]), compareIssue)
    --print "#{basename} is clean"
    return true
  else
    --print "#{basename} is dirty"
    return false

parseFile = (results, counts, fp, lastchanged, watch) ->
  basename = fp\gsub(path.currentdir! .. SEPARATOR, "")
  mtime = file.modified_time(fp)

  if mtime > (lastchanged[basename] or 0)
    result, count = Parse.File(fp) -- actually parse through it

    if results[basename]
      result.clean = cleanIssue(results, result, basename)

    results[basename] = result
    counts[basename] = count
  else
    --print(basename, 'is set clean!')
    results[basename].clean = true

  lastchanged[basename] = mtime

class Actions -- damned lawyers
  new: (@maytag, @args, @config) =>
    --convert relative paths to full paths
    @results = {}
    @counts = {}
    @modified = {}
    for k, v in pairs(@config.ignore)
      if not @config.ignore[k]\find('%.')
        @config.ignore[k] = path.join(path.currentdir!, v)
    for k, v in pairs(@config.files)
      @config.files[k] = path.join(path.currentdir!, v)

  invoke: () =>
    if @args.help then
      print @maytag.help
    elseif @args.watch then
      @watch!
    elseif @args.update or @args.push then
      @dirs!
      @files!
      @update!
    else
      @dirs!
      @files(true)

  dirs: () =>
    --scan dirs, ignore if in ignore
    for i, d in ipairs @config.dirs
      files = dir.getallfiles(path.join(path.currentdir!, d))
      for j, fpath in ipairs files
        if path.isfile(fpath) and Parse.Comment[path.extension(fpath)]
          @scanfile(fpath)

  scanfile: (file) =>
    ignored = false
    for _, ignore in ipairs @config.ignore
      if path.common_prefix(file, ignore) == ignore\lower! then
        ignored = true
      if not ignored and dir.fnmatch(file, ignore) then
        ignored = true
    if not ignored
      @config.files[#@config.files + 1] = file

  --scan files
  -- [todo@gdansk] - refactor file parser, it's messy.

  files: (output, watch, writer) =>
    for i, fp in ipairs @config.files
      if path.isfile(fp) and Parse.Comment[path.extension(fp)]
        parseFile(@results, @counts, fp, @modified, watch)

    if output
      Printer(@results, @config, @counts, @modified, writer)

  watch: () =>
    status, socket = pcall(require, 'socket')
    if not status
      print "LuaSocket not found. Aborting..."
      os.exit(-1)

    @config.show = 'dirty'

    --hidden run so that all files are dirty to start
    --@dirs!
    --@files!

    print "Watching in #{path.currentdir!}"
    print "Press Ctrl+C to exit."
    with io.open('.issues', 'w')
      \close!

    while true
      issues = io.open('.issues', 'a')

      --re-enumerate files
      @config.files = {}
      @dirs!

      @files(true, true, issues\write)

      collectgarbage()
      issues\close!
      socket.sleep(DEFAULT_SLEEP_TIME)

  update: () =>
    status, Bitbucket = pcall(require, 'Bitbucket')
    if not status
      print "#{Bitbucket} not found. Aborting..."
      os.exit(-1)

    remote = Bitbucket(@config, @results)
    remote\login!
    remote\get!
    Printer(@results, @config, @counts, @modified)
    if @args.push
      remote\post!
