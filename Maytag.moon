-- Maitag inline tagger
-- Tag anything you want
app     = require 'pl.app'
lapp    = require 'pl.lapp'
Config  = require 'Config'
Actions = require 'Actions'

Maytag =
  name: "Maitag"
  version: "0.2.1"
  author: "gdansk"
  url: "http://bitbucket.org/gdansk/maitag"

Maytag.help = "    Maitag " .. Maytag.version .. [[ usage: maitag [OPTION]...
    Without any arguments Maitag will look to the .maitag file
    If no .maitag file exists it will create one.

    -d, --dirs (optional string)     Directories to scan.
    -f, --files (optional string)    Files to scan.
    -i, --ignore (optional string)   Directories to ignore.
    -b, --sort (optional string)     Sort by (a)lpha, (m)odified, or issues (c)ount.
    -s, --show (optional string)     Show all/dirty/clean tags.
    -t, --tags (optional string)     Define tags.
    -r, --remote (optional string)   Remote reposity service (Bitbucket).
    -l, --login (optional string)    Remote username.
    -p, --project (optional string)  Remote project name.
    -u, --update                     Update issues in remote.
    -P, --push                       Push new issues to remote.
    -w, --watch                      Watch for file changes.
    -h, --help                       Print this help.\n]]

Maytag.main = () ->
  app.require_here!
  args = lapp(Maytag.help)
  config = Config(args)
  action = Actions(Maytag, args, config)
  action\invoke!
  config\write!

return Maytag
-- [minor:todo] - show user's issues
