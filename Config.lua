require('pl.stringx').import()
local file = require('pl.file')
local pretty = require('pl.pretty')
local parseTags
parseTags = function(tags)
  if type(tags) == 'table' then
    return tags
  else
    local res = tags:split(';')
    if #res == 0 then
      res[1] = tags
    end
    return res
  end
end
local defaultPath = '.maitag'
local defaultConfig = [[{
  dirs = "",
  ignore = "",
  files = "",
  show = "all",
  sort = "a",
  tags = "todo;review;fix",
  priority = {
    critical = "red underline",
    major = "yellow",
    minor = "green dim"
  },
  remote = "Bitbucket",
  login = nil, -- username
  project = nil -- project name
}]]
local Config
do
  local _base_0 = {
    init = function(self, arguments, defaults)
      defaults = defaults or { }
      for k, v in pairs(arguments) do
        if arguments[k] == '' and defaults[k] then
          arguments[k] = nil
        end
      end
      self.show = arguments.show or defaults.show or 'all'
      if self.show ~= 'all' and self.show ~= 'dirty' and self.show ~= 'clean' then
        self.show = 'all'
      end
      self.sort = arguments.sort or defaults.sort or 'a'
      self.remote = arguments.remote or defaults.remote
      self.login = arguments.login or defaults.login
      self.project = arguments.project or defaults.project
      do
        local _tbl_0 = { }
        local _list_0 = parseTags(arguments.tags or defaults.tags or "todo;review;fix")
        for _index_0 = 1, #_list_0 do
          local k = _list_0[_index_0]
          _tbl_0[k] = true
        end
        self.tags = _tbl_0
      end
      self.dirs = parseTags(arguments.dirs or defaults.dirs or "")
      self.ignore = parseTags(arguments.ignore or defaults.ignore or "")
      self.files = parseTags(arguments.files or defaults.files or "")
      self.priority = parseTags(arguments.priority or defaults.priority or {
        critical = 'red',
        major = 'green',
        minor = 'blue'
      })
    end,
    write = function(self)
      if not self.conf then
        return file.write(defaultPath, defaultConfig)
      end
    end
  }
  _base_0.__index = _base_0
  local _class_0 = setmetatable({
    __init = function(self, args)
      local contents = file.read('.maitag')
      if contents then
        self.conf = assert(pretty.read(contents))
        return self:init(args, self.conf)
      else
        self:init(args)
        return file.write('.maitag', defaultConfig)
      end
    end,
    __base = _base_0,
    __name = "Config"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  Config = _class_0
  return _class_0
end
