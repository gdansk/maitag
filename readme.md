###### Maitag: a stupidly simple issue manager

![alt text](https://i.imgur.com/5Y6rJMx.png "example usage")

Maitag depends on [Luasocket](https://github.com/diegonehab/luasocket)(optionally), [Luasec](https://github.com/brunoos/luasec)(optionally) and [LuaFileSystem](http://keplerproject.github.io/luafilesystem/)(mandatory). These can be obtained through [LuaRocks](http://luarocks.org). Maitag is a simple Lua program that scans files looking for tags, priorities and assignees. It can watch a directory for changes. Maitag can upload these issues to a issue service (currently this only Bitbucket), or import issues from a remote repository.

###### Usage
In order to create issues Maitag looks for tags. A tag looks like this (replace the word 'tag' with your tag's name):

```lua
-- A tag is defined in a comment in the form:
-- [tag] - message
-- Here's an example:
-- [todo] - show some examples!

-- Be warned: the message continues ONLY to the end of the line.
-- Assign a tag to any user name by following the tag with '@username'.
-- [tag@username] - message
-- [todo@szensk] - show some examples!

-- Assign a priority by preceding the tag with 'priority:'
-- [priority:tag] - message
-- [critical:todo] - show some examples!

-- You can combine them as you please
-- [critical:todo@szensk] - show some examples!
```

###### Command Line
maitag [OPTIONS]...
If `.maitag` file exists, it will load that first. Any command line arguments will override the `.maitag` configuration file.

```
-d, --dirs (optional string)     Directories to scan.
-f, --files (optional string)    Files to scan.
-i, --ignore (optional string)   Directories to ignore.
-b, --sort (optional string)     Sort by (a)lpha, (m)odified, or issues (c)ount.
-s, --show (optional string)     Show all/dirty/clean tags.
-t, --tags (optional string)     Define tags.
-r, --remote (optional string)   Remote reposity service (Bitbucket).
-l, --login (optional string)    Remote username.
-p, --project (optional string)  Remote project name.
-u, --update                     Update issues in remote.
-P, --push                       Push new issues to remote.
-w, --watch                      Watch for file changes.
-h, --help                       Print help.
```

###### Watcher

To invoke watch mode execute `maitag -w` or `maitag --watch`. When in watch mode maitag runs continually. It will rescan anyfile you save (even within new subdirectories). Then the resultant issues are writen to the file `.issues`. I recommend having this open in your text editor and returning to it whenever you finish a task.

###### Configuration
The `.maitag` configuration file is written in Lua (as a table). As an example:

```lua
{
  dirs = "src",
  ignore = "bin",
  files = ".moon;.lua",
  show = "all",
  sort = "a",
  tags = "todo;review;fix",
  priority = {
    critical = "red underline",
    major = "yellow",
    minor = "green dim"
  },
  remote = "Bitbucket",
  login = nil, -- username
  project = nil -- remote project name
}
```
