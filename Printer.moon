--Printer.moon
acolor = require 'ansicolors.ansicolors'
path   = require 'pl.path'
config = nil
files  = nil
orderedFiles = nil
issuecount   = nil
modified     = nil
writer       = io.stdout\write
removeKeys   = nil

-- sort functions
alphabetic   = (a, b) -> (a < b)
countsort    = (a, b) -> (issuecount[a] or 0) > (issuecount[b] or 0)
modifiedsort = (a, b) -> (modified[a] or 0) > (modified[b] or 0)

colors = (s) ->
  acolor(s, removeKeys)

-- print buffer
printf = (file, str) ->
  out = files[file]
  out[#out+1] = str

format = (character, color) ->
  colors("%{#{color}}[ #{character} ]") .. colors(" %{#{color}}")

printMessage = (file, priority, message, assign, resolve, line) ->
  p = config.priority[priority] and colors("%{#{config.priority[priority]}}#{priority}: ") or ""
  msg = "        #{p}#{line}\'#{message}\'#{assign}"
  msg ..= " #{colors('%{green}' .. ((resolve and ' [' .. resolve .. ']') or ''))}"
  printf(file, msg)

printTag = (file, tag) ->
  printf(file, colors('    %{blue}' .. tag))

printFile = (file, clean, counts) ->
  files[file] = {}
  count = counts[file]
  postfix = count > 1 and "s" or ""
  color = clean and format('o', 'green') or format('x', 'red')
  printf(file, "#{color}#{file}" .. (count > 0 and "\t#{format(count .. ' issue' .. postfix, 'cyan dim')}" or ""))

-- [todo@gdansk] - test sort by modify
-- [major:review] - a good idea to sort by modified time
Printer = (results, conf, counts, lastchanged, filewriter) ->
  removeKeys = filewriter
  filewriter = filewriter or writer
  files, orderedFiles = {}, {}
  --select sort
  sort = alphabetic
  if conf.sort == 'c'
    sort = countsort
  elseif conf.sort == 'm'
    sort = modifiedsort

  config = conf
  issuecount = counts
  modified = lastchanged or modified

  for file, tags in pairs results
    --print only what config.show allows
    if conf.show == "all" or (conf.show == "dirty" and not tags.clean) or (conf.show == "clean" and tags.clean)
      orderedFiles[#orderedFiles+1] = file
      printFile(file, tags.clean, counts)
      for tag, issues in pairs tags
        if tag ~= 'clean' and conf.tags[tag]
          printTag(file, "[ #{tag} ]: ")
          for k, issue in pairs issues
            assign = issue.assign and issue.assign ~= "" and colors("%{dim blue} #{issue.assign}")
            line = issue.line and issue.line ~= "" and colors("%{dim}line #{issue.line} ")
            printMessage(file, issue.priority, issue.message, assign or "", issue.resolve, line or "")

  --print in sorted order
  table.sort(orderedFiles, sort)
  for i, filename in pairs orderedFiles
    table.insert(files[filename], "")
    filewriter(table.concat(files[filename], '\n'))

return Printer
