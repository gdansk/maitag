local socket = require('socket')
local url = require('socket.url')
local https = require('ssl.https')
local json = require('dkjson.dkjson')
local Printer = require('Printer')
local colors = require('ansicolors.ansicolors')
local Bitbucket
do
  local _base_0 = {
    login = function(self)
      if not self.config.project then
        io.write("Project name:")
        self.config.project = io.read()
      end
      if not self.config.login then
        io.write("User name:")
        self.config.login = io.read()
      end
      if os.getenv("SHELL") then
        os.execute("stty -echo")
      end
      io.write("Password for " .. tostring(self.config.login) .. ":")
      local password = io.read()
      if os.getenv("SHELL") then
        os.execute("stty echo")
      end
      self.request = {
        scheme = 'https',
        user = self.config.login,
        password = password,
        host = "bitbucket.org",
        path = "/api/1.0/repositories/" .. tostring(self.config.login) .. "/" .. tostring(self.config.project) .. "/"
      }
      if password == "" then
        self.request.password, self.request.user = nil, nil
      end
      return self:validateRepository()
    end,
    validateRepository = function(self)
      local command = url.build(self.request)
      local result, code = https.request(command)
      if code ~= 200 then
        return print(colors("%{red} [ x ] %{red}invalid or unauthorized repository"))
      else
        return print(colors("%{green} [ o ] %{green}valid repository"))
      end
    end,
    postIssue = function(self, issue, file)
      local jsonIssue = {
        responsible = issue.assign,
        priority = issue.priority,
        status = issue.resolve,
        content = "maitag: [" .. tostring(issue.tag) .. "] - " .. tostring(file) .. " | line " .. tostring(issue.line),
        kind = "task"
      }
      local query = {
        "title=" .. tostring(url.escape(issue.message .. ' [' .. file .. ']'))
      }
      for k, v in pairs(jsonIssue) do
        if v ~= "" then
          query[#query + 1] = "&" .. tostring(k) .. "=" .. tostring(url.escape(tostring(v)))
        end
      end
      query = table.concat(query, "")
      local r, c = https.request(self.command, query)
      if c ~= 200 then
        return print("Error:", r)
      end
    end,
    post = function(self)
      print("Submitting to https://bitbucket.org/" .. tostring(self.config.login) .. "/" .. tostring(self.config.project) .. "/issues")
      self.request.path = "/api/1.0/repositories/" .. tostring(self.config.login) .. "/" .. tostring(self.config.project) .. "/issues/"
      self.command = url.build(self.request)
      for file, tags in pairs(self.results) do
        for tag, issues in pairs(tags) do
          if tag ~= "clean" then
            for i, issue in pairs(issues) do
              if not issue.resolve then
                self:postIssue(issue, file)
              end
            end
          end
        end
      end
    end,
    getIssue = function(self, issue)
      local _, tag, filepath, line
      _, _, tag, filepath, line = issue.content:find("^maitag:%s?%[(.-)%]%s?%-%s?(.-)%s|%sline%s(.-)$")
      if filepath then
        local assign = issue.responsible and issue.responsible.username
        local title
        _, _, title = issue.title:find("(.-)%s?%[")
        title = title or issue.title
        local file = self.results[filepath]
        if not file then
          self.results[filepath] = {
            clean = false
          }
          file = self.results[filepath]
        end
        file.clean = false
        if issue.priority == "" then
          issue.priorty = nil
        end
        if not file[tag] then
          file[tag] = { }
        end
        file[tag][line .. title] = {
          message = title,
          priority = issue.priority,
          resolve = issue.status,
          assign = assign,
          tag = tag,
          line = tonumber(line)
        }
      end
    end,
    get = function(self)
      self.request.path = "/api/1.0/repositories/" .. tostring(self.config.login) .. "/" .. tostring(self.config.project) .. "/issues/"
      local command = url.build(self.request)
      local res, code = https.request(command)
      res = json.decode(res)
      for i, issue in ipairs(res.issues) do
        self:getIssue(issue)
      end
    end
  }
  _base_0.__index = _base_0
  local _class_0 = setmetatable({
    __init = function(self, config, results)
      if results == nil then
        results = { }
      end
      self.config, self.results = config, results
    end,
    __base = _base_0,
    __name = "Bitbucket"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  Bitbucket = _class_0
end
return Bitbucket
