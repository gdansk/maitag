local Printer = require('Printer')
local Parse = require('Parser')
local path = require('pl.path')
local dir = require('pl.dir')
local file = require('pl.file')
local tablex = require('pl.tablex')
local DEFAULT_SLEEP_TIME = 2
local SEPARATOR = package.config:sub(1, 1)
local getIssues
getIssues = function(iss)
  local result = { }
  if not iss or iss.clean == nil then
    return result
  end
  for tag, v in pairs(iss) do
    if tag ~= 'clean' then
      for msg, m in pairs(v) do
        result[#result + 1] = m
      end
    end
  end
  return result
end
local compareIssue
compareIssue = function(a, b)
  return a.priority == b.priority and a.tag == b.tag and a.assign == b.assign and a.message == b.message
end
local cleanIssue
cleanIssue = function(results, result, basename)
  if tablex.compare_no_order(getIssues(result), getIssues(results[basename]), compareIssue) then
    return true
  else
    return false
  end
end
local parseFile
parseFile = function(results, counts, fp, lastchanged, watch)
  local basename = fp:gsub(path.currentdir() .. SEPARATOR, "")
  local mtime = file.modified_time(fp)
  if mtime > (lastchanged[basename] or 0) then
    local result, count = Parse.File(fp)
    if results[basename] then
      result.clean = cleanIssue(results, result, basename)
    end
    results[basename] = result
    counts[basename] = count
  else
    results[basename].clean = true
  end
  lastchanged[basename] = mtime
end
local Actions
do
  local _base_0 = {
    invoke = function(self)
      if self.args.help then
        return print(self.maytag.help)
      elseif self.args.watch then
        return self:watch()
      elseif self.args.update or self.args.push then
        self:dirs()
        self:files()
        return self:update()
      else
        self:dirs()
        return self:files(true)
      end
    end,
    dirs = function(self)
      for i, d in ipairs(self.config.dirs) do
        local files = dir.getallfiles(path.join(path.currentdir(), d))
        for j, fpath in ipairs(files) do
          if path.isfile(fpath) and Parse.Comment[path.extension(fpath)] then
            self:scanfile(fpath)
          end
        end
      end
    end,
    scanfile = function(self, file)
      local ignored = false
      for _, ignore in ipairs(self.config.ignore) do
        if path.common_prefix(file, ignore) == ignore:lower() then
          ignored = true
        end
        if not ignored and dir.fnmatch(file, ignore) then
          ignored = true
        end
      end
      if not ignored then
        self.config.files[#self.config.files + 1] = file
      end
    end,
    files = function(self, output, watch, writer)
      for i, fp in ipairs(self.config.files) do
        if path.isfile(fp) and Parse.Comment[path.extension(fp)] then
          parseFile(self.results, self.counts, fp, self.modified, watch)
        end
      end
      if output then
        return Printer(self.results, self.config, self.counts, self.modified, writer)
      end
    end,
    watch = function(self)
      local status, socket = pcall(require, 'socket')
      if not status then
        print("LuaSocket not found. Aborting...")
        os.exit(-1)
      end
      self.config.show = 'dirty'
      print("Watching in " .. tostring(path.currentdir()))
      print("Press Ctrl+C to exit.")
      do
        local _with_0 = io.open('.issues', 'w')
        _with_0:close()
      end
      while true do
        local issues = io.open('.issues', 'a')
        self.config.files = { }
        self:dirs()
        self:files(true, true, (function()
          local _base_1 = issues
          local _fn_0 = _base_1.write
          return function(...)
            return _fn_0(_base_1, ...)
          end
        end)())
        collectgarbage()
        issues:close()
        socket.sleep(DEFAULT_SLEEP_TIME)
      end
    end,
    update = function(self)
      local status, Bitbucket = pcall(require, 'Bitbucket')
      if not status then
        print(tostring(Bitbucket) .. " not found. Aborting...")
        os.exit(-1)
      end
      local remote = Bitbucket(self.config, self.results)
      remote:login()
      remote:get()
      Printer(self.results, self.config, self.counts, self.modified)
      if self.args.push then
        return remote:post()
      end
    end
  }
  _base_0.__index = _base_0
  local _class_0 = setmetatable({
    __init = function(self, maytag, args, config)
      self.maytag, self.args, self.config = maytag, args, config
      self.results = { }
      self.counts = { }
      self.modified = { }
      for k, v in pairs(self.config.ignore) do
        if not self.config.ignore[k]:find('%.') then
          self.config.ignore[k] = path.join(path.currentdir(), v)
        end
      end
      for k, v in pairs(self.config.files) do
        self.config.files[k] = path.join(path.currentdir(), v)
      end
    end,
    __base = _base_0,
    __name = "Actions"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  _base_0.__class = _class_0
  Actions = _class_0
  return _class_0
end
