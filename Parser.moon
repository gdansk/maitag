--Parser.moon
-- langauges: lua, moon, ruby, python
Printer = require 'Printer'
path    = require 'pl.path'

Comment =
  ['.lua']:  '%-%-' --lua
  ['.moon']: '%-%-' --moonscript
  ['.ms']:   '%-%-' --moonscript alternative
  ['.rb']:   '#'    --ruby
  ['.py']:   '#'    --python
  ['.cpp']:  '//'   --c++

-- returns elements in order of priority, assignment, tag
find_elements = (tag) ->
  priority, ptag, assign = tag\gfind("(%w*):?(%w*)@?(%w*)")!
  if ptag ~= ""
    return priority, assign, ptag
  elseif assign ~= ""
    if tag\find(':')
      return priorty, nil, ptag
    else
      return nil, assign, priority
  return nil, nil, priority

Parser =
  File: (filepath) ->
    count = 0
    linec = 0
    res = { clean: true } -- table of issues for filepath where k = tag
    dir, ext = path.splitext(filepath)

    pattern = "#{Comment[ext]}%s?%[(%w*:?%w+@?%w*)%]%s?[%-:]%s?(.-)$"

    for line in io.lines(filepath)
      linec += 1
      _, __, tag, message = line\find(pattern)
      continue unless tag
      priority, assign, tag = find_elements(tag)

      res.clean = false
      res[tag] = {} unless res[tag]
      count += 1
      res[tag][linec..message] = {priority: priority, tag: tag, assign: assign, message: message, line: linec}

    return res, count
  Comment: Comment

return Parser
