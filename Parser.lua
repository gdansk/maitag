local Printer = require('Printer')
local file = require('pl.file')
local path = require('pl.path')
local Comment = {
  ['.lua'] = '%-%-',
  ['.moon'] = '%-%-',
  ['.ms'] = '%-%-',
  ['.rb'] = '#',
  ['.py'] = '#',
  ['.cpp'] = '//'
}
local find_elements
find_elements = function(tag)
  local priority, ptag, assign = tag:gfind("(%w*):?(%w*)@?(%w*)")()
  if ptag ~= "" then
    return priority, assign, ptag
  elseif assign ~= "" then
    if tag:find(':') then
      return priorty, nil, ptag
    else
      return nil, assign, priority
    end
  end
  return nil, nil, priority
end
local Parser = {
  File = function(filepath)
    local count = 0
    local res = {
      clean = true
    }
    local contents = assert(file.read(filepath))
    local ext
    filepath, ext = path.splitext(filepath)
    local pattern = tostring(Comment[ext]) .. "%s?%[(%w*:?%w+@?%w*)%]%s?[%-:]%s?(.-)\n"
    for tag, message in contents:gfind(pattern) do
      local priority, assign
      priority, assign, tag = find_elements(tag)
      res.clean = false
      if not res[tag] then
        res[tag] = { }
      end
      count = count + 1
      res[tag][message] = {
        priority = priority,
        tag = tag,
        assign = assign,
        message = message
      }
    end
    return res, count
  end,
  Comment = Comment
}
return Parser
